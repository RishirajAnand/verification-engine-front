(function() {
  'use strict';

  angular
    .module('yoGulpAngular', ['ngAnimate', 'ngCookies', 'ngTouch',
      'ngSanitize', 'ngMessages', 'ngAria', 'toastr', 'ngRoute',
      'ngPrettyJson', 'ngFileUpload', 'ngMaterial', 'ngPrettyJson'
    ]);

})();
