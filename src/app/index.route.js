(function() {
  'use strict';


  angular
    .module('yoGulpAngular')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($routeProvider, $locationProvider) {
    $routeProvider
      .when('/test', {
        templateUrl: 'app/main/main.html'


      });
    $locationProvider.html5Mode(true);
  }

})();
