(function() {
  'use strict';

  angular
    .module('yoGulpAngular')
    .controller('MainController', MainController).directive('onReadFile',
      function($parse) {
        return {
          restrict: 'A',
          scope: false,
          link: function(scope, element, attrs) {
            element.bind('change', function(e) {

              var onFileReadFn = $parse(attrs.onReadFile);
              var reader = new FileReader();

              reader.onload = function() {
                var fileContents = reader.result;
                scope.$apply(function() {
                  onFileReadFn(scope, {
                    'contents': fileContents
                  });
                });
              };
              reader.readAsText(element[0].files[0]);
            });
          }
        };
      });

  /** @ngInject */
  function MainController($scope, $http, $timeout) {
    var vm = this;
    $scope.loading = false;
    $scope.showtable = false;
    $scope.displayFileContents = function(contents) {
      $scope.results = contents;
      $scope.loading = true;
      $scope.showtable = false;
      $scope.error = "";
      //$scope.jsonStr = JSON.stringify($scope.results, undefined, 2);
      $scope.parsedJson = $scope.safelyParseJSON($scope.results);
      var saveurl = "http://0.0.0.0:51783/api/saves";
      var geturl =
        "http://0.0.0.0:51783/api/callBacks?requestId=";
      if ($scope.parsedJson != undefined) {


        $http.post(saveurl, $scope.parsedJson, {
          headers: {
            'Content-Type': 'application/json'
          }
        }).then(function(response) {

          console.log("Got the save response", response);
          console.log(response.data.result.requestId);
          $scope.requestId = response.data.result.requestId;
          $timeout(function() {
            $http.get(geturl + $scope.requestId).then(function(
              response) {
              $scope.jsonStr = JsonHuman.format(response.data);
              $scope.loading = false;

              var myEl = angular.element(document.querySelector(
                '#output'));
              myEl.empty();

              myEl.append($scope.jsonStr);
              myEl.children("table").css('overflow:scroll');
              $scope.showtable = true;

            }, function(response) {
              console.log(
                "Failed to get verification response:",
                response);

              $scope.loading = false;
              $scope.error = response.data.error.message;
            });
          }, 10000);

        }, function(response) {
          console.log("Failed to do save", response);
          $scope.error = response.data.error.message;
          $scope.showtable = false;
          $scope.loading = false;
        });
      } else {
        $scope.error =
          "Entered file is not in json format, Please enter the correct JSON";
        $scope.showtable = false;
        $scope.loading = false;

      }



    };

    $scope.safelyParseJSON = function(json) {
      // This function cannot be optimised, it's best to
      // keep it small!
      var parsed

      try {
        parsed = JSON.parse(json)
      } catch (e) {
        console.log("Not a json");
        $scope.error = "Entered file is not a json";
      }

      return parsed // Could be undefined!
    }
  }
})();
